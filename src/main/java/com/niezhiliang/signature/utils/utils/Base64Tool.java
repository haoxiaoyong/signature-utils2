package com.niezhiliang.signature.utils.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class Base64Tool {

    public static byte[] buildBytes(BufferedImage image){

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //bufferedImage转为byte数组
        try {
            ImageIO.write(image, "png", outStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outStream.toByteArray();
    }
}
