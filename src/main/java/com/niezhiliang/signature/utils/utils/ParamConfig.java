package com.niezhiliang.signature.utils.utils;

import com.niezhiliang.signature.utils.SealCircle;
import com.niezhiliang.signature.utils.SealConfiguration;
import com.niezhiliang.signature.utils.SealFont;
import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class ParamConfig {

    private static SealConfiguration SealConfiguration(String companyName, ColorEnum colorEnum, FontEnum fontEnum) {
        SealConfiguration configuration = new SealConfiguration();
        /**
         * 主文字
         */
        SealFont mainFont = new SealFont();
        mainFont.setBold(true);
        mainFont.setFontFamily(fontEnum.getFont());
        mainFont.setMarginSize(5);
        mainFont.setFontText(companyName);
        mainFont.setFontSize(30);
        mainFont.setFontSpace(30.0);
        if (companyName.length() > 14) {
            mainFont.setFontSize(23);
            mainFont.setFontSpace(21.0);
        }
        configuration.setMainFont(mainFont);
        // centerText(fontEnum, configuration);

        /**
         * 图片大小
         */
        configuration.setImageSize(250);
        /**
         * 背景颜色
         */
        configuration.setBackgroudColor(colorEnum.getColor());
        /**
         * 边线粗细、半径
         */
        configuration.setBorderCircle(new SealCircle(4, 115, 115));

        return configuration;
    }


    public static SealConfiguration centerText(String companyName, ColorEnum colorEnum, FontEnum fontEnum) {
        SealConfiguration configuration = SealConfiguration(companyName, colorEnum, fontEnum);
        /**
         * 中心文字
         */
        SealFont centerFont = new SealFont();
        centerFont.setBold(false);
        centerFont.setFontFamily(fontEnum.getFont());
        centerFont.setFontText("★");
        centerFont.setFontSize(70);
        configuration.setCenterFont(centerFont);
        return configuration;
    }

}
