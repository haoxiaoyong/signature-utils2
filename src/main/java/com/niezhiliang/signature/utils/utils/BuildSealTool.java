package com.niezhiliang.signature.utils.utils;

import com.niezhiliang.signature.utils.SealConfiguration;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class BuildSealTool {

    private static Graphics2D g2d;


    private final static int INIT_BEGIN = 10;

    public void setG2d(Graphics2D g2d) {
        this.g2d = g2d;
    }

    public BufferedImage buildSeal(SealConfiguration conf) throws Exception {
        //1.画布
        BufferedImage bi = new BufferedImage(conf.getImageSize(), conf.getImageSize(), BufferedImage.TYPE_4BYTE_ABGR);

        //2.画笔
        Graphics2D g2d = bi.createGraphics();

        //2.1抗锯齿设置
        //文本不抗锯齿，否则圆中心的文字会被拉长
        RenderingHints hints = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
        //其他图形抗锯齿
        hints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHints(hints);

        //2.2设置背景透明度
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 0));

        //2.3填充矩形
        g2d.fillRect(0, 0, conf.getImageSize(), conf.getImageSize());

        //2.4重设透明度，开始画图
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1));

        //2.5设置画笔颜色
        g2d.setPaint(conf.getBackgroudColor());

        //3.画边线圆
        if (conf.getBorderCircle() != null) {
            if (conf.getBorderCircle() == null) {
                return null;
            }
            //1.圆线条粗细默认是圆直径的1/35
            int lineSize = conf.getBorderCircle().getLineSize() == null ? conf.getBorderCircle().getHeight() * 2 / (35) : conf.getBorderCircle().getLineSize();

            //2.画圆
            g2d.setStroke(new BasicStroke(lineSize));
            g2d.drawOval(INIT_BEGIN, INIT_BEGIN, conf.getBorderCircle().getWidth() * 2, conf.getBorderCircle().getHeight() * 2);
            setG2d(g2d);
        } else {
            throw new Exception("BorderCircle can not null！on line 69");
        }
        return bi;
    }


    /**
     * 绘制公司名称->drawStarted
     *
     * @param conf
     */
    public static BufferedImage drawCompanyName(SealConfiguration conf) {
        int borderCircleWidth = conf.getBorderCircle().getWidth();
        int borderCircleHeight = conf.getBorderCircle().getHeight();
        BufferedImage bi = null;
        try {
            if (borderCircleHeight == borderCircleWidth) {
                BuildSealTool build = new BuildSealTool();
                bi = build.buildSeal(conf);
                //画主文字
                DrawArcTool.drawArcFont4Circle(g2d, borderCircleHeight, conf.getMainFont(), true);
                //画星
                DrawArcTool.drawFont(g2d, (borderCircleWidth + INIT_BEGIN) * 2, (borderCircleHeight + INIT_BEGIN) * 2, conf.getCenterFont());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bi;
    }

}
