package com.niezhiliang.signature.utils.abs;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 *
 * 创建抽象产品类,定义抽象产品的公共接口
 * 椭圆
 */
public abstract class AbstractEllipseSeal extends AbstractBasicSeal {


    public abstract void drawStarted();

    @Override
    public abstract void drawSimple();

    @Override
    public abstract void drawComplex();
}
