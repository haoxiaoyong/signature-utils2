package com.niezhiliang.signature.utils.abs;

import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * <p>
 * 创建抽象产品族类 ，定义具体产品的公共接口；
 */
public abstract class AbstractBasicSeal {

    public String prefix = "data:image/png;base64,";

    /**
     * 绘制普通签章
     */
    public abstract String drawStarted(String companyName, ColorEnum colorEnum, FontEnum fontEnum);

    /**
     * 绘制带有抬头文字的签章
     */
    public abstract void drawSimple();

    /**
     * 绘制带有抬头文字和副文字的签章
     */
    public abstract void drawComplex();


}
