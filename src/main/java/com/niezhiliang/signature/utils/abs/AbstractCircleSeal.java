package com.niezhiliang.signature.utils.abs;

import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 *
 *  创建抽象产品类,定义抽象产品的公共接口
 *  圆弧
 */
public abstract class AbstractCircleSeal extends AbstractBasicSeal {

    @Override
    public abstract String drawStarted(String companyName, ColorEnum colorEnum, FontEnum fontEnum);

    @Override
    public abstract void drawSimple();

    @Override
    public abstract void drawComplex();
}
