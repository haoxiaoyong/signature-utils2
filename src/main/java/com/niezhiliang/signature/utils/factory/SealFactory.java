package com.niezhiliang.signature.utils.factory;

import com.niezhiliang.signature.utils.abs.AbstractBasicSeal;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 *
 * 创建抽象工厂类，定义具体工厂的公共接口
 */
public abstract class SealFactory {

    protected AbstractBasicSeal basicSeal;

    public abstract AbstractBasicSeal newInstance();
}
