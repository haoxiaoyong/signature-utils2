package com.niezhiliang.signature.utils.factory;

import com.niezhiliang.signature.utils.abs.AbstractBasicSeal;
import com.niezhiliang.signature.utils.container.CircleSeal;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class CircleSealFactory extends SealFactory {

    @Override
    public AbstractBasicSeal newInstance() {
        if (this.basicSeal == null) {
            return new CircleSeal();
        }
        return this.basicSeal;
    }
}
