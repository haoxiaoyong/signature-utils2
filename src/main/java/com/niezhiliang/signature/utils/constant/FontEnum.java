package com.niezhiliang.signature.utils.constant;


/**
 * @Author NieZhiLiang
 * @Email nzlsgg@163.com
 * @Date 2019/2/12 上午10:56
 */
public enum  FontEnum {

    //SONGTI("宋体"),//SimSun
    SONGTI("SimSun"),
    //FANGZHENGHEITI("方正黑体"),SimHei
    FANGZHENGHEITI("SimHei"),
    //KAITI("楷体");KaiTi
    KAITI("KaiTi");
    ;
    private String font;

    FontEnum(String font) {
        this.font = font;
    }

    public String getFont() {
        return font;
    }
}
