package com.niezhiliang.signature.utils.container;

import com.niezhiliang.signature.utils.abs.AbstractEllipseSeal;
import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 *
 * 创建具体产品类（继承抽象产品类,定义生产的具体产品)
 * 画椭圆
 */
public class EllipseSeal extends AbstractEllipseSeal {

    @Override
    public void drawStarted() {

    }

    @Override
    public String drawStarted(String companyName, ColorEnum colorEnum, FontEnum fontEnum) {
        return null;
    }

    @Override
    public void drawSimple() {

    }

    @Override
    public void drawComplex() {

    }
}
