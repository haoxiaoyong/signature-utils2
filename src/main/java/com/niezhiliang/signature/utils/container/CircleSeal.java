package com.niezhiliang.signature.utils.container;

import com.niezhiliang.signature.utils.SealConfiguration;
import com.niezhiliang.signature.utils.abs.AbstractCircleSeal;
import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;
import com.niezhiliang.signature.utils.utils.Base64Tool;
import com.niezhiliang.signature.utils.utils.BuildSealTool;
import com.niezhiliang.signature.utils.utils.ParamConfig;
import org.apache.tomcat.util.codec.binary.Base64;

import java.awt.image.BufferedImage;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 * <p>
 * 创建具体产品类（继承抽象产品类,定义生产的具体产品)
 * 画圆弧
 */
public class CircleSeal extends AbstractCircleSeal {

    @Override
    public String drawStarted(String companyName, ColorEnum colorEnum, FontEnum fontEnum) {
        SealConfiguration sealConfiguration = ParamConfig.centerText(companyName, colorEnum, fontEnum);
        BufferedImage bufferedImage = BuildSealTool.drawCompanyName(sealConfiguration);
        byte[] bytes = Base64Tool.buildBytes(bufferedImage);
        return this.prefix + Base64.encodeBase64String(bytes);
    }

    @Override
    public void drawSimple() {

    }

    @Override
    public void drawComplex() {

    }


}
