import com.niezhiliang.signature.utils.SealUtils;
import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;
import org.junit.Test;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class TestSeal {

    @Test
    public void test1() throws Exception {
        String base64 = SealUtils.companyCircleSeal("浙江杭州江干下沙某某某网络集团有限公司", ColorEnum.RED, FontEnum.FANGZHENGHEITI,"1234567899876","合同专用");
        System.out.println(base64);
    }
    @Test
    public void test2() throws Exception {
        String base64 = SealUtils.companyEllipseSeal("浙江某某某网络集团有限公司",ColorEnum.RED,FontEnum.SONGTI,"1234567899876","神域专用");
        System.out.println(base64);
    }
}
