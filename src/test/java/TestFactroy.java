import com.niezhiliang.signature.utils.abs.AbstractBasicSeal;
import com.niezhiliang.signature.utils.constant.ColorEnum;
import com.niezhiliang.signature.utils.constant.FontEnum;
import com.niezhiliang.signature.utils.factory.CircleSealFactory;
import org.junit.Test;

/**
 * Created by Haoxy on 2019-04-23.
 * E-mail:hxyHelloWorld@163.com
 * github:https://github.com/haoxiaoyong1014
 */
public class TestFactroy {

    @Test
    public void test1() {

        AbstractBasicSeal seal = new CircleSealFactory().newInstance();
        String baseStr = seal.drawStarted("浙江杭州江干下沙某某某网络集团有限公司", ColorEnum.RED, FontEnum.KAITI);
        System.out.println(baseStr);
    }
}
